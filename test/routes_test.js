const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	

	it('Test post currency if it is running', () => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			
		})
	})

	it('Test Currency post if it returns 400 if no name property',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'sampleAlias',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('Test currency post if it returns 400 if name is not a string ',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'sampleAlias',
			name: 123,
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('Test currency post if name is an empty string',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'sampleAlias',
			name: "",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('Test currency post if it returns 400 if no ex property',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'sampleAlias',
			name: "sampleName"
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('Test currency post if it returns 400 if ex property is not an object',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'sampleAlias',
			name: "sampleName",
			ex: 123
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('Test currency post if it returns_400 if ex property is an empty object',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'sampleAlias',
			name: "sampleName",
			ex: {}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('Test currency post if it returns 400 if alias is missing',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			name: "sampleName",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('Test currency post if it returns 400 if alias is not string',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 123,
			name: "sampleName",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('Test currency post if it returns 400 if alias is empty',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "",
			name: "sampleName",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('Test currency post if it returns 400 if there is a duplicate alias',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "won",
			name: "South Korean Won",
			ex: {
				'peso': 0.043,
				'usd': 0.00084,
				'yen': 0.092,
				'yuan': 0.0059
			}	
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('Test currency post if it returns 200 if there is no duplicate',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			done();
		})
	})
})


